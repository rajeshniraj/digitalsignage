CREATE DATABASE  IF NOT EXISTS `digital_signage_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `digital_signage_db`;

--
-- Table structure for table `userdetails`
--

DROP TABLE IF EXISTS `userdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userdetails` (
  `USERDETID_NR` double NOT NULL,
  `USERID_NR` double NOT NULL,
  `MENUNAME_TT` char(50) NOT NULL,
  PRIMARY KEY (`USERDETID_NR`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userdetails`
--

LOCK TABLES `userdetails` WRITE;
/*!40000 ALTER TABLE `userdetails` DISABLE KEYS */;
INSERT INTO `userdetails` VALUES (1,2,'Network Manager'),(2,2,'Layout Manager'),(3,2,'Media Manager'),(4,2,'Ticker'),(5,2,'Clock'),(6,2,'Weather'),(7,2,'Report'),(8,2,'User'),(9,2,'Alert'),(10,2,'Player Screenshot'),(11,2,'Bluetooth'),(12,2,'Update'),(13,2,'LiveTV'),(122,1,'LiveTV'),(121,1,'Update'),(120,1,'Bluetooth'),(119,1,'Player Screenshot'),(118,1,'Alert'),(117,1,'User'),(116,1,'Report'),(115,1,'Weather'),(114,1,'Clock'),(113,1,'Ticker'),(112,1,'Audio'),(111,1,'Media Manager'),(110,1,'Layout Manager'),(109,1,'Network Manager'),(123,6,'Network Manager'),(124,6,'Layout Manager'),(125,6,'Media Manager'),(126,6,'Audio'),(127,6,'Ticker'),(128,6,'Clock'),(129,6,'Weather'),(130,6,'Report'),(131,6,'User'),(132,6,'Alert'),(133,6,'Player Screenshot'),(134,6,'Bluetooth'),(135,6,'Update'),(136,6,'LiveTV');
/*!40000 ALTER TABLE `userdetails` ENABLE KEYS */;
UNLOCK TABLES;

