package com.three.m.digital.signage.view;

import javax.annotation.Resource;
import java.io.Serializable;

/**
 * Created by Karthik on 13-09-2015.
 */
@Resource
public class LoginStatus implements Serializable {
    private String userId;
    private final String email;
    private final boolean authenticated;

    public LoginStatus(String userId, String email, boolean authenticated) {
        this.userId = userId;
        this.email = email;
        this.authenticated = authenticated;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }
}
