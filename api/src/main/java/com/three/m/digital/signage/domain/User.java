package com.three.m.digital.signage.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by Karthik on 04-07-2015.
 */
@Entity
@Table(name = "usermaster")
public class User {

    @Id
    @Column(name = "USERID_NR")
    private Double id;

    @Column(name = "USERNAME_TT")
    private String name;

    @Column(name = "USERTYPE_TT")
    private String type;

    @Column(name = "PASSWORD_TT")
    private String password;

    @Column(name = "FULLNAME_TT")
    private String fullName;

    @Column(name = "EMAIL_TT")
    private String email;

    @Column(name = "ISACTIVE_TT")
    private String isActive;

    @Column(name = "ADDED_NR")
    private Double added;

    @Column(name = "MODIFIED_NR")
    private Double modified;

    @Column(name = "DELETED_NR")
    private Double deleted;

    @Column(name = "ADDEDDATE_DT")
    private Date addedDate;

    @Column(name = "MODIFIEDDATE_DT")
    private Date modifiedDate;

    @Column(name = "DELETEDDATE_DT")
    private Date deletedDate;

    @Column(name = "NETWORKID_NR")
    private Integer networkId;

    @Column(name = "LASTLOGIN_DT")
    private String lastLoginDate;

    @Column(name = "EMAILALERT_TT")
    private String emailAltert;

    @Column(name = "PHONE_TT")
    private String phone;

    public Double getId() {
        return id;
    }

    public void setId(Double id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public Double getAdded() {
        return added;
    }

    public void setAdded(Double added) {
        this.added = added;
    }

    public Double getModified() {
        return modified;
    }

    public void setModified(Double modified) {
        this.modified = modified;
    }

    public Double getDeleted() {
        return deleted;
    }

    public void setDeleted(Double deleted) {
        this.deleted = deleted;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public Integer getNetworkId() {
        return networkId;
    }

    public void setNetworkId(Integer networkId) {
        this.networkId = networkId;
    }

    public String getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(String lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public String getEmailAltert() {
        return emailAltert;
    }

    public void setEmailAltert(String emailAltert) {
        this.emailAltert = emailAltert;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
