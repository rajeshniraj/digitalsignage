package com.three.m.digital.signage.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Karthik on 04-07-2015.
 */
@Entity
@Table(name = "userdetails")
public class UserDetails {
    @Column(name = "userid_nr")
    private Double userId;

    @Id
    @Column(name = "userdetid_nr")
    private Double userDetailsId;

    @Column(name = "menuname_tt")
    private String menuName;


    public Double getUserId() {
        return userId;
    }

    public void setUserId(Double userId) {
        this.userId = userId;
    }

    public Double getUserDetailsId() {
        return userDetailsId;
    }

    public void setUserDetailsId(Double userDetailsId) {
        this.userDetailsId = userDetailsId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }
}
