package com.three.m.digital.signage.dao;

import com.three.m.digital.signage.domain.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Karthik on 19-04-2015.
 */
@Transactional
@RepositoryRestResource(collectionResourceRel = "userdetails", path = "userdetails")
public interface UserDetailsRepository extends JpaRepository<UserDetails, Double> {
    List<UserDetails> findByMenuName(@Param("menu") String menuName);
}
