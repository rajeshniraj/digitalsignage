package com.three.m.digital.signage.dao;

import com.three.m.digital.signage.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Karthik on 19-04-2015.
 */
@Transactional
@RepositoryRestResource(collectionResourceRel = "users", path = "users")
public interface UserRepository extends JpaRepository<User, Double> {
    List<User> findByEmail(@Param("email") String emailAddress);
    User findByNameAndPassword(@Param("user")String userId, @Param("password")String password);
}
